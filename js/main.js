
var app = angular.module('testApp', ["ngRoute"])
  .config(function ($routeProvider) {
    $routeProvider
      // Login
      .when("/", { templateUrl: "partials/login.html", controller: "LoginCtrl" })
      .when("/login", { templateUrl: "partials/login.html", controller: "LoginCtrl" })
      // Home
      .when("/home", { templateUrl: "partials/home.html", controller: "HomeCtrl" })
      // View Records
      .when("/records/clinics", { templateUrl: "partials/clinics.html", controller: "RecordsCtrl" })
      .when("/records/staff", { templateUrl: "partials/staff.html", controller: "RecordsCtrl" })
      // 404
      .when("/404", { templateUrl: "partials/404.html"})
      // Default
      .otherwise({redirectTo:'/404'});
  });

app.controller("BaseCtrl", function ($scope, $location) {
  $scope.isLogin = function () {
    return ($location.path() == "/login" || $location.path() == "/");
  }
});

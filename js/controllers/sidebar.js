
app.controller("SidebarCtrl", function ($scope, $location) {
  $scope.links = [];

  var getLinks = function () {
    $scope.links.push({ name: "Clinics", path: "/records/clinics" });
    $scope.links.push({ name: "Staff", path: "/records/staff" });
    $scope.links.push({ name: "Customers", path: "/records/customers" });
    $scope.links.push({ name: "Treatments", path: "/records/treatments" });
    $scope.links.push({ name: "Examinations", path: "/records/examinatins" });
    $scope.links.push({ name: "Appointments", path: "/records/appointments" });
    $scope.links.push({ name: "Invoices", path: "/records/invoices" });
    // Stretch Goals
    $scope.links.push({ name: "Supplies", path: "/records/supplies" });
    $scope.links.push({ name: "Pens", path: "/records/pens" });
  }

  $scope.changeUrl = function (path) {
    $('.ui.sidebar').sidebar('toggle');

    $location.path(path);
  }

  // $(".ui.pointing.dropdown").dropdown({
  //   on: "hover"
  // });

  $("#logout-modal").modal({
    onApprove: function () {
      window.alert("Successfully Logged Out\n(Note: This feature has not yet been implemented)");
      // $location.path("/login");
    }
  });

  $scope.logout = function () {
    $("#logout-modal").modal("show");
  }

  getLinks();
});

app.controller('RecordsCtrl', function ($scope) {
  $('.ui.accordion').accordion({
    exclusive: false
  });

  $('.ui.fluid.dropdown').dropdown();
  $('table').tablesort();

  $scope.toggleSidebar = function () {
    $('.ui.sidebar').sidebar('toggle');
  }

  $scope.removeFilter = function (icon) {
    $(icon)
      .closest('.label')
      .transition('fade');
  }
});

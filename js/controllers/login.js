
// Login Controller
app.controller("LoginCtrl", function ($scope, $location, $window) {
  // Reload the route to avoid sidebar bug
  if ($('.ui.sidebar').length > 0) {
    $window.location.reload();
  }

  $scope.user = {};

  var loginForm = $(".ui.form");

  // Set grid height after loading
  $("#login-grid").height("100%");

  $scope.login = function () {
    if (!loginForm.form("validate form")) {
      return;
    }

    console.log($scope.user);
    $location.path("/home");
  }

  loginForm.submit(function (e) {
      e.preventDefault(e);

      $scope.login();
  });
  
  loginForm.form({
    on: "blur", 
    fields: {
      email: {
        identifier: "email",
        rules: [
          {
            type: "empty",
            prompt: "Please enter an email"
          },
          {
            type: "email",
            promopt: "Please enter a valid email"
          }
        ]
      },
      password: {
        identifier: "password",
        rules: [
          {
            type: "empty",
            prompt: "Please enter a password"
          },
          {
            type: "minLength[6]",
            prompt: "Your password must be at least {ruleValue} characters"
          }
        ]
      }
    }
  });
});

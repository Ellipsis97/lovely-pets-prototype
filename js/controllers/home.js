
app.controller("HomeCtrl", function ($scope) {
  $scope.toggleSidebar = function () {
    $('.ui.sidebar').sidebar('toggle');
  }

  $scope.closeMessage = function (icon) {
    $(icon)
      .closest('.message')
      .transition('fade');
  }
});

Lovely Pets Prototype Application
=================================

This application is meant to function as prototype for the final product
released at the end of IFB399 Cpastone. Certain functionality may be
missing or changed from the final release.

# Dependencies
- [JQuery (3.2.1)](http://api.jquery.com/)
- [AngularJS (1.6.4)](https://angularjs.org/)
- [AngularJS Route (1.6.4)](https://angularjs.org/)
- [Semantic UI (2.2.10)](https://semantic-ui.com/)
- [Less CSS (2.7.2)](http://lesscss.org/)